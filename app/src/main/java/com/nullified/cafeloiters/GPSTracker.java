package com.nullified.cafeloiters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GPSTracker extends Service implements LocationListener {

    private final Context mContext;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;
    double latitude; // latitude
    double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 20; // 10 meters

    private static final long MIN_PROXIMITY = 10;
    private static final long MIN_TIME_PROXIMITY = 60000;

    Map<String, String> prevLocation = new HashMap<String, String>();
    SurveyedLocation mDbHelper;
    SQLiteDatabase db;
    Location location; // location
    PersistentSurvey dialog;


    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 2; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public GPSTracker(Context context) {
        this.mContext = context;
        mDbHelper = new SurveyedLocation(mContext);

        db = mDbHelper.getWritableDatabase();
        this.dialog = new PersistentSurvey();
        getLocation();
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            60000,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                60000,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){
        if(locationManager != null){
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public static double strToDouble(String str) {
        double r = (double) Math.round(Double.valueOf(str) * 1000) / 1000;
        Log.v("CONVERTED", " = " + r);
        return r;
    }

    public static String doubleToStr(double str) {
        String r = String.valueOf(str);
        Log.v("CONVERTED", " = " + r);
        return r;
    }

    public void showSurvey(final String lat,final  String lng) {
        SurveyedLocation getSurvey = new SurveyedLocation(mContext);

        Cursor data = getSurvey.getNearLocation(Double.valueOf(lat), Double.valueOf(lng));

        boolean sameLocation = false;
        Log.v("DATA_IS_NULL?", " " + data);


        if (data != null) {

            //more to the first row
            data.moveToFirst();

            //iterate over rows
            for (int i = 0; i < data.getCount(); i++) {
                double rLat = Double.valueOf(data.getString(data.getColumnIndex("lat")));
                double rLng = Double.valueOf(data.getString(data.getColumnIndex("lng")));
                for(String x: data.getColumnNames()) {
                    Log.v("DATA", x + " =  " + data.getString(data.getColumnIndex(x)));

                }

                Location storedLocation = new Location("storedLocation");
                storedLocation.setLatitude(rLat);
                storedLocation.setLongitude(rLng);
                Log.v("PROXIMITY", "ALERT "  + location.distanceTo(storedLocation));
                if(location.distanceTo(storedLocation) <= 5) {
                    sameLocation = true;
                }

                data.moveToNext();
            }

            data.close();
        }



        if(!sameLocation) {
            if(!dialog.isShown()) {
                // request store data //
                ((MainActivity)mContext).loadHashedUrl(
                        "#request_store/" + String.valueOf(getLatitude())+ "," + String.valueOf(getLongitude())
                );
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //((MainActivity)mContext).loaderUrl();
        Date now = new Date();
        String prevLat = prevLocation.get("lat");
        String prevLng = prevLocation.get("lng");
        Long prevTime = Long.parseLong("10000");

        if(prevLocation.get("time") != null) {
            prevTime = Long.parseLong(prevLocation.get("time"));

        }
        Long curTime = now.getTime();
        if(prevLat != null && prevLng != null) {
            double latitude=Double.parseDouble(prevLat);
            double longitude=Double.parseDouble(prevLng);
            float distance=0;
            Location prevLocate=new Location("prevLocate");
            prevLocate.setLatitude(latitude);
            prevLocate.setLongitude(longitude);
            long timeElapse = curTime-prevTime;

            if(location.distanceTo(prevLocate) <= MIN_PROXIMITY && timeElapse > MIN_TIME_PROXIMITY ) {


                showSurvey(
                        String.valueOf(getLatitude()),
                        String.valueOf(getLongitude())
                );
            } else if (location.distanceTo(prevLocate) > MIN_PROXIMITY) {
                ((MainActivity)mContext).loadHashedUrl(
                        "#location/" + String.valueOf(getLatitude())+ "," + String.valueOf(getLongitude())
                );
            }
            if(prevTime==null || timeElapse > MIN_TIME_PROXIMITY) {
                prevLocation.put("time", String.valueOf(curTime));

                Log.v("onLocationChanged", getLocation().toString());
                Log.v("onLocationChanged", "changes?");
            }

        }

        Log.v("onLocationChanged", getLocation().toString());

        prevLocation.put("lat", String.valueOf(getLatitude()));
        prevLocation.put("lng", String.valueOf(getLongitude()));


    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}