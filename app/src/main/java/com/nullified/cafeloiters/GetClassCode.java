package com.nullified.cafeloiters;

import java.util.HashMap;

public class GetClassCode {
    static HashMap<String, String> codeHash = new HashMap<String, String>();

    public static void setCodeHash(String id, String title) {
        codeHash.put(id, title);
    }


    public static String getCode(String param) {
        return codeHash.get(param);
    }
}
