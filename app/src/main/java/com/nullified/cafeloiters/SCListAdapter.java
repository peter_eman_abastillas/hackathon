package com.nullified.cafeloiters;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * Created by PETEREMAN.ABASTILLAS on 6/13/2015.
 */
public class SCListAdapter extends ArrayAdapter {

    private Context context;
    private boolean useList = true;
    private String type;

    public SCListAdapter(Context context, ArrayList<SCListItems> users, String type) {
        super(context, 0, users);
        this.type = type;
        this.context = context;
        Log.e("GEN_LIST_ITEM", " " + users);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        SCListItems user = (SCListItems) getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        Log.e("GEN_LIST_ITEM", " " + convertView);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_item_list, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvHome = (TextView) convertView.findViewById(R.id.tvHometown);
        // Populate the data into the template view using the data object
        tvName.setText(user.name);
        tvHome.setText(user.id);
        if(type == "Friends") {
            String url = "https://graph.facebook.com/" + user.id + "/picture?type=square";
            loadBitmap(url, convertView);
        }
        // Return the completed view to render on screen
        return convertView;
    }

    public void loadBitmap(String url, View convertView) {
        new DownloadImageTask((ImageView) convertView.findViewById(R.id.ivUserIcon))
                .execute(url);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
