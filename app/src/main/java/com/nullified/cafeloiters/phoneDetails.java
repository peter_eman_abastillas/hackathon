package com.nullified.cafeloiters;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ImageView;

import com.facebook.Profile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by PETEREMAN.ABASTILLAS on 6/4/2015.
 */
public class phoneDetails  {
    private String mPhoneNumber;
    private String mImei;
    private String mTopic;
    private JSONObject mProfile;
    private JSONObject mLocation;
    Context mContext;

    public phoneDetails(Context mContext){
        setMdn(mContext);
        setImei(mContext);
    }

    public void setProfile(Profile profile) throws JSONException {
        mProfile = new JSONObject();
        mProfile.put("name", profile.getName());
        mProfile.put("profileUri", profile.getProfilePictureUri(30,30));
        mProfile.put("id", profile.getId());
    }

    private void setMdn(Context mContext) {
        TelephonyManager tMgr = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        mPhoneNumber = tMgr.getLine1Number();
    }
    private void  setImei(Context mContext) {
        TelephonyManager tMgr = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        mImei = tMgr.getDeviceId();
    }
    public String getMdn() {
        return mPhoneNumber ;
    }
    public String  getImei() {
        return mImei;
    }
    public void setLoc(String lat, String lng) throws JSONException  {
        mLocation = new JSONObject();
        mLocation.put("lat", lat);
        mLocation.put("lng", lng);
    }

    public void setTopic(String topic) {
        mTopic = topic;
    }

    public String getUsersDetails() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("mdn", getMdn());
        obj.put("imei", getImei());
        obj.put("facebook", mProfile);
        obj.put("location", mLocation);
        Log.v("getUsersDetails", " " + mProfile);
        return obj.toString();
    }

    public String getUserLocation() throws JSONException {
        JSONObject obj = new JSONObject();

        obj.put("location", mLocation);
        Log.v("getUserLocation", " " + mProfile);
        return obj.toString();

    }

}
