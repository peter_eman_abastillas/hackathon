package com.nullified.cafeloiters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TabHost;

import org.json.JSONArray;

/**
 * Created by PETEREMAN.ABASTILLAS on 6/22/2015.
 */
public class GroupDialogFragment extends DialogFragment {
    static final int NUM_ITEMS = 3;
    private JSONArray jsonFriendsArray;
    private PagerTabStrip pagerTabStrip;
    Dialog customDialog;
    FragmentTransaction fragmentTransaction;
    private Context mContext;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.tab_dialog, container, false);
        fragmentTransaction = getChildFragmentManager().beginTransaction();

        return rootView;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        customDialog = new Dialog(getActivity());
        customDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(R.layout.tab_dialog);
        return customDialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        fragmentTransaction.replace(R.layout.tab_dialog, this);
        fragmentTransaction.add(R.id.myFragmentHolder, new TabsFragment());
        fragmentTransaction.commit();
        super.onActivityCreated(savedInstanceState);

    }
}
