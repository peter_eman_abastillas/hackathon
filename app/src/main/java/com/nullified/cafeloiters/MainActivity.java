package com.nullified.cafeloiters;

import java.net.URLEncoder;
import java.util.Arrays;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityGroup;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;

import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.util.Log;
import android.widget.TabHost;

import com.facebook.AccessTokenTracker;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity {

    CallbackManager callbackManager;

    private AccessToken accessToken;
    private ProfileTracker profileTracker;
    private AccessTokenTracker accessTokenTracker;
    private LoginResult loginResultCache;
    private Boolean isLogin;
    public final String mapUrl = "http://inappqa01-map.azurewebsites.net/";
    WebView webview;
    private static final String TAG = "Logging";
    private Profile profile;
    private Location location;
    private LocationManager locationManager;
    private JSONArray jsonFriendsArray;
    private GraphResponse graphFriendsResponse;
    String lng;
    String lat;
    GPSTracker gps;
    Bundle activityInstance;

    ViewPager mViewPager;



    public boolean isLoggedIn() {
        Log.i(TAG, "Get Profile " + (profile != null));
        if(accessToken == null) {
        }

        if ( profile == null) {
            profile = Profile.getCurrentProfile();
            Log.i(TAG, "Get Profile " + (profile != null));
        }

        return (accessToken != null || profile != null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        // setContentView(R.layout.loading_screen);
        activityInstance = savedInstanceState;

        isLogin = isLoggedIn();

        if(!isLogin) {
            doFBLogin();
        } else {
            initWebView();
        }

    }

    private void doFBLogin() {
        setContentView(R.layout.facebook_login);

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));


        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "onSuccess");
                loginResultCache = loginResult;
                accessToken = loginResultCache.getAccessToken();
                profile = Profile.getCurrentProfile();

                initWebView();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "onCancel ");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.d(TAG, "onError " + exception);
            }
        });
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                // App code
            }
        };
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // App code
            }
        };


    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    protected void initWebView () {
        setContentView(R.layout.web_view);
        gps = new GPSTracker(this);
        String loc;
        webview = (WebView) findViewById(R.id.webView);

        Log.i(TAG, " Profile.getId() " + profile.getId());

        if (gps.canGetLocation()) {
            lat = String.valueOf(gps.getLatitude());
            lng = String.valueOf(gps.getLongitude());
            loc = "" + String.valueOf(gps.getLatitude()) + "+" + String.valueOf(gps.getLongitude());
            Log.i(TAG, loc);
        } else {
            loc = "0+0";
        }

        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setAllowFileAccess(true);
        webview.getSettings().setAllowFileAccessFromFileURLs(true);
        webview.getSettings().setDomStorageEnabled(true);
        webview.setWebViewClient(new MyWebViewClient());
        // Below required for geolocation
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setGeolocationEnabled(true);
        webview.addJavascriptInterface(new jsInterface(), "jsInterface");

        webview.setWebChromeClient(new GeoWebChromeClient());
        String str = "null";
        phoneDetails usersDetails = new phoneDetails(activity);
        try {
            usersDetails.setTopic("LOAD_PROFILE");
            usersDetails.setProfile(profile);
            usersDetails.setLoc(lat, lng);
            str = usersDetails.getUsersDetails();
        } catch (JSONException e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
        str = "#user/" +URLEncoder.encode(str);
        Log.i("LOAD_URL" , mapUrl + str);

        webview.loadUrl(mapUrl + str);
    }

    public void loadHashedUrl(String url) {

        webview.loadUrl("javascript:plugData('" + url + "');");
        Log.i("LOAD_URL", "javascript:plugData('" + url + "');");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(callbackManager!= null)
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(isLoggedIn()) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_not_login_main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.fb_login_button:
                doFBLogin();
                return true;
            case R.id.group_settings:
                GroupDialogFragment groupDialogFragment = new GroupDialogFragment();
                groupDialogFragment.show(this.getSupportFragmentManager(), "Dialog");
                return true;
            case R.id.clear_cache:
                webview.clearCache(true);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    final Activity activity = this;
    final Context mContext = this;


    /*public void createTab( ) {
        Dialog df = new Dialog(this);
        df.setCancelable(true);
        df.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        df.setContentView(R.layout.tab_dialog);

        LocalActivityManager mLocalActivityManager = new LocalActivityManager(activity, false);
        Resources ressources = getResources();
        mLocalActivityManager.dispatchCreate(activityInstance);
        TabHost tabs =(TabHost) df.findViewById(R.id.tabHost);
        // Add tabs


        Intent friendsList  = new Intent(this, GroupFragmentTab.class);
        tabs.setup(mLocalActivityManager);

        TabHost.TabSpec tabpage1 = tabs.newTabSpec("one")
                .setIndicator("Groups")
                .setContent(friendsList);

        TabHost.TabSpec tabpage2 = tabs.newTabSpec("two");
        tabpage2.setContent(R.id.shareGroup);
        tabpage2.setIndicator("Friends");

        tabs.addTab(tabpage1);
        tabs.addTab(tabpage2);
    }*/


    public class GeoWebChromeClient extends WebChromeClient {
        @Override
        public void onGeolocationPermissionsShowPrompt(String origin,
                                                       GeolocationPermissions.Callback callback) {
            // Always grant permission since the app itself requires location
            // permission and the user has therefore already granted it
            callback.invoke(origin, true, false);
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        public void onProgressChanged(WebView view, int progress) {
            // Activities and WebViews measure progress with different scales.
            // The progress meter will automatically disappear when we reach 100%
            // activity.setProgress(progress * 1000);
            Log.v(TAG, " " + progress);
            if(progress == 100) {
            }
        }
        /*@Override
        public void onReceivedTitle*/
    }

    public class MyWebViewClient extends WebViewClient {
        private int webViewPreviousState;
        private final int STARTED = 1;
        private final int STOP = 1;
        private final int REDIRECTED = 4;

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            webViewPreviousState = STARTED;

        }
        public void onPageFinished(WebView view, String url) {

            if (webViewPreviousState == STARTED) {

            }

        }

    }

    public class jsInterface {

        @JavascriptInterface
        public String getUserData() {
            String str = "null";
            phoneDetails usersDetails = new phoneDetails(activity);
            try {
                usersDetails.setProfile(profile);
                str = usersDetails.getUsersDetails().toString();
                Log.v("Profile json? ", str);
            } catch (JSONException e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
            return str;
        }

        @JavascriptInterface
        public void broadcastData(String type, String dataArray) throws JSONException {
            try {
                JSONObject obj = new JSONObject(dataArray);
                PersistentSurvey ps = new PersistentSurvey();
                if(!ps.isShown()) {
                    switch (type) {
                        case "STORE_INFORMATION":
                            String lat = obj.getString("lat");
                            String lng = obj.getString("lng");
                            JSONArray edata = obj.getJSONArray("data");
                            ps.initPesistentDialog(mContext, lat, lng, edata);
                            break;
                        case "ALL_STORE_INFORMATION":
                            String elat = obj.getString("lat");
                            String elng = obj.getString("lng");
                            JSONArray data = obj.getJSONArray("data");

                            ps.initPesistentDialog(mContext, elat, elng, data);
                            break;
                    }
                }
            } catch (JSONException t ) {
                Log.e("JSONObject", t.getLocalizedMessage() + "Could not parse malformed JSON: \"" + dataArray + "\" From " + type );
            }

        }


    }
}
