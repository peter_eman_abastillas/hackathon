package com.nullified.cafeloiters;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by PETEREMAN.ABASTILLAS on 6/19/2015.
 */
public class TestRest extends AsyncTask<String, String, String> {
    private String baseUrl;
    private String api;
    private Fragment mContext;


    public void setBaseUrl(String url) {
        this.baseUrl = url;
    }
    
    public void setApi(String api) {
        this.api = api;
    }
    public void setContext(GroupFragmentTab mContext) {

        // this.mContext = mContext;
    }

    @Override
    protected String doInBackground(String... params) {
        String urlString = baseUrl+api; // URL to call
        String resultToDisplay = "";
        StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;
        InputStream in = null;
            // HTTP Get
        try {
            URL url = new URL(urlString);
            Log.v("TEST_REST", urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            resultToDisplay = responseStrBuilder.toString();

            Log.v("TEST_REST", resultToDisplay);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }

        return resultToDisplay;
    }

    @Override
    protected void onPostExecute(String result) {


    }

}