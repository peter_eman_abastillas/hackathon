package com.nullified.cafeloiters;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by PETEREMAN.ABASTILLAS on 6/5/2015.
 */
public class PersistentSurvey {
    Dialog d;
    Context mContext;
    SQLiteDatabase db;
    // ArrayList<String> storeList;
    String[] storeList;
    JSONObject payload;
    String lat;
    String lng;
    String editTextValue;
    HashMap<String, String> storeHash = new HashMap<String, String>();

    public void initPesistentDialog(Context m, String lat, String lng, JSONArray stores) {
        String address = "";

        try {
            storeList = new String[stores.length()];
            for (int i = 0; i < stores.length(); i++) {
                Log.v("STORE_LOCATION", "++++++++++++++++++++++" + stores.getJSONObject(i));
                if(i==0) address = stores.getJSONObject(i).getString("title");
                storeHash.put(
                        stores.getJSONObject(i).getString("title"),
                        stores.getJSONObject(i).getString("id")
                );
                storeList[i] = stores.getJSONObject(i).getString("title");

            }
        } catch (JSONException e) {
            Log.e("JSONObject", e.getLocalizedMessage() + "Could not parse malformed JSON: \"" + stores + "\"" );

        }

        initDialog(m, lat, lng, address);
    }

    public void initDialog(Context m, String lat, String  lng, String address) {
        String loc_address = "";
        try {
            payload = new JSONObject();
            payload.put("lat", lat);
            payload.put("lng", lng);
            if(address != "") {
                    JSONObject json = new JSONObject(address);
                    payload.put("title", json.getString("title"));
                    payload.put("actual_address", json.getString("address"));
                    payload.put("id", json.getString("id"));
                    loc_address = json.getString("title");
            } else {
            }
        } catch (JSONException e) {
            loc_address = address;
            Log.v("loc_address==", loc_address);
        }
        this.mContext = m;
        this.d = new Dialog(this.mContext);
        this.lat = lat;
        this.lng = lng;
        if (!d.isShowing()) {
            d.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
            d.setContentView(R.layout.persistent_survey);
            d.setCancelable(true);
            LinearLayout srolable = (LinearLayout)  d.findViewById(R.id.emoji_container);
            //there are a lot of settings, for dialog, check them all out!

            Field[] drawables = com.nullified.cafeloiters.R.drawable.class.getFields();
            for (Field f : drawables) {
                try {
                    if(f.getName().toLowerCase().contains("emoji".toLowerCase())) {
                        int resourceId = mContext.getResources().getIdentifier(f.getName(), "drawable", mContext.getPackageName());
                        createImages(resourceId, srolable, f.getName());
                        Log.v("DRAWABLES?", "R.drawable." + f.getName());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            TextView text = (TextView) d.findViewById(R.id.TextView01);
            text.setText(R.string.lots_of_text);

            final EditText editText = (EditText) d.findViewById(R.id.location_editor);
            Log.v("loc_address", loc_address);
            editText.setText("" + loc_address, TextView.BufferType.EDITABLE);
            editText.setSelected(false);
            editText.setFocusable(false);
            editTextValue = loc_address;
            final Spinner spinner = (Spinner) d.findViewById(R.id.spinner);
            if(storeList.length > 0) {
                ArrayAdapter<String> adapter2 = new CustomAdapter<String>(mContext, android.R.layout.simple_spinner_item, storeList);
                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter2);
            } else {
                ArrayAdapter<CharSequence> adapter1 = CustomAdapter.createFromResource(mContext, R.array.store_array, android.R.layout.simple_spinner_item);
                adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter1);
            }
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                           long arg3) {
                    String mSupplier = spinner.getSelectedItem().toString();
                    editText.setText("" + mSupplier, TextView.BufferType.EDITABLE);
                    editTextValue = mSupplier;
                    ((TextView)arg1).setText(null);
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {

                }
            });
            d.show();
        }
    }

    public boolean isShown() {
        if(d == null) return false;
        return d.isShowing();
    }
    public void createImages(int resourceId, LinearLayout srolable, final String getName) throws JSONException {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View custom = inflater.inflate(R.layout.tpl_emoji, null);

        ImageButton btnGreen = (ImageButton) custom.findViewById(R.id.temoji);
        btnGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)  {
                    try {
                        String[] emo = getName.split("_");
                        payload.put("warning_type", emo[1]);
                        payload.put("rate", emo[2]);
                        payload.put("emoji", getName);
                        payload.put("placeId", storeHash.get(editTextValue));
                        payload.put("address", editTextValue);

                        Log.v("SAVE_ADDRESS", payload.toString());
                    } catch (JSONException e) {

                    }
                    saveLocationData(lat, lng, payload);
                    d.hide();
                }
            }

            );
            btnGreen.setImageResource(resourceId);
            btnGreen.setId(resourceId);

            if(srolable.getParent()!=null)((ViewGroup)btnGreen.getParent()).

            removeView(btnGreen);

            srolable.addView(btnGreen);
        }

    public void saveLocationData(String lat, String lng, final JSONObject jsonObject) {

        ContentValues values = new ContentValues();
        String id = "";
        String emoji = "none";
        String comment = "none";
        String address = "none";
        String warning_type = "0";
        String rate = "0";
        Handler mainHandler = new Handler(mContext.getMainLooper());

        try {
            id = jsonObject.getString("placeId");
            emoji = jsonObject.getString("emoji");
            comment = jsonObject.getString("id");
            address = jsonObject.getString("address");
            warning_type = jsonObject.getString("warning_type");
            rate = jsonObject.getString("rate");
            Log.v("JSON", jsonObject.toString());
        } catch (JSONException e) {

        }
        Double dLat = Double.valueOf(lat);
        Double dLng = Double.valueOf(lng);
        Log.v("PERSISTENT_DIALOG", " " + mContext);

        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                ((MainActivity)mContext).loadHashedUrl(
                        "#location_details/" + jsonObject.toString()
                );
            }
        });


        values.put(SurveyedLocation.SurveyEntry.LOCATION_lAT_ENTRY_ID, lat);
        values.put(SurveyedLocation.SurveyEntry.LOCATION_LNG_ENTRY_ID, lng);
        values.put(SurveyedLocation.SurveyEntry.LOCATION_WARNING_TYPE, warning_type);
        values.put(SurveyedLocation.SurveyEntry.LOCATION_RATE, rate);
        values.put(SurveyedLocation.SurveyEntry.LOCATION_EMOJI_ENTRY_ID, emoji);
        values.put(SurveyedLocation.SurveyEntry.LOCATION_PLACE_ID, id);
        values.put(SurveyedLocation.SurveyEntry.LOCATION_ADD_ENTRY_ID, address);
        values.put(SurveyedLocation.SurveyEntry.COSLAT, Math.cos(SurveyedLocation.deg2rad(dLat)));
        values.put(SurveyedLocation.SurveyEntry.SINLAT, Math.sin(SurveyedLocation.deg2rad(dLat)));
        values.put(SurveyedLocation.SurveyEntry.COSLNG, Math.cos(SurveyedLocation.deg2rad(dLng)));
        values.put(SurveyedLocation.SurveyEntry.SINLNG, Math.sin(SurveyedLocation.deg2rad(dLng)));
        values.put(SurveyedLocation.SurveyEntry.LOCATION_COMMENT_ENTRY_ID, comment);
        if(db==null) {
            SurveyedLocation mDbHelper = new SurveyedLocation(mContext);
            db = mDbHelper.getWritableDatabase();
        }
        long newRowId;
        newRowId = db.insert(
                SurveyedLocation.SurveyEntry.TABLE_NAME,
                SurveyedLocation.SurveyEntry.COLUMN_NAME_NULLABLE,
                values
        );
        Log.v("CURSOR", "===== " + newRowId);

    }
    private static class CustomAdapter<T> extends ArrayAdapter<String> {
        public CustomAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText("");
            return view;
        }
    }

}
