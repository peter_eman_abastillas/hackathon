package com.nullified.cafeloiters;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.support.v4.app.ListFragment;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.manavo.rest.RestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by PETEREMAN.ABASTILLAS on 6/19/2015.
 */
public class GroupListFragment extends ListFragment  {
    SCListAdapter newList;
    private String mTag;
    private int mTotal;
    public GroupListFragment() {
    }

    public GroupListFragment(String tag) {
        mTag = tag;

        Log.d("GROUP_TAB", "Constructor: tag=" + tag);
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.groups_fragment_list, container, false);
        TestRest rest = new TestRest();
        rest.setBaseUrl("http://inappqa01-map.azurewebsites.net/api/");
        rest.setApi("groups.txt");

        rest.execute();


        Log.v("REST_STATUS", rest.getStatus().toString());
        try {
            JSONObject jsonArray = populateUsersList();
            JSONArray jsonData = new JSONArray(jsonArray.getString("list").toString());
            ArrayList<SCListItems> arrayOfGroups = SCListItems.setGroups(jsonData);
            Log.e("LIST", " "+ arrayOfGroups);
            newList = new SCListAdapter(getActivity(), arrayOfGroups, "Groups");

            setListAdapter(newList);
            newList.notifyDataSetChanged();

        } catch (JSONException e) {

        }
        return rootView;
    }
    public static void getJsonExtra(String result) throws JSONException {

    }


    private JSONObject populateUsersList() throws JSONException {
        // Construct the data source
        JSONObject obj;
        String jsonString = "{\n" +
                "    \"list\": [\n" +
                "        {\n" +
                "            \"name\": \"test group\",\n" +
                "            \"id\": \"yut4uy-b43hj24-432u4-bj324780\",\n" +
                "            \"status\": \"invite\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"ATM weekend\",\n" +
                "            \"id\": \"yut4uy-ds34f67-432u4-bj324780\",\n" +
                "            \"status\": \"joined\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"Vegan Adventure\",\n" +
                "            \"id\": \"yut4uy-fghjhgff-432u4-bj324780\",\n" +
                "            \"status\": \"joined\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"Joined Group\",\n" +
                "            \"id\": \"yut4uy-dsfsdfsdf-432u4-bj324780\",\n" +
                "            \"status\": \"joined\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"total\": 2\n" +
                "}";
            obj = new JSONObject(jsonString);


        return obj;
    }
}
