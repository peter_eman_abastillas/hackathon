package com.nullified.cafeloiters;

import android.app.Activity;

import com.manavo.rest.RestApi;

/**
 * Created by PETEREMAN.ABASTILLAS on 6/19/2015.
 */
public class GroupsRestAPI extends RestApi {
    public GroupsRestAPI(Activity activity) {
        super(activity);
        this.urlSuffix = ".txt";
        this.BASE_URL = "http://inappqa01-map.azurewebsites.net/api/";
        this.rest.setHost("inappqa01-map.azurewebsites.net");
        this.rest.setPort(80);
        this.setUserAgent("sprinter");

        this.acceptAllSslCertificates();

    }
    public void getGroups() {
        this.get("groups");
    }
    public void getGroup(String id) {
        this.get("groups/"+id);
    }
}
