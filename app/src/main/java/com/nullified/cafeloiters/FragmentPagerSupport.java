package com.nullified.cafeloiters;

import android.app.ActionBar;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by PETEREMAN.ABASTILLAS on 5/26/2015.
 */
public class FragmentPagerSupport extends FragmentActivity  {
    static final int NUM_ITEMS = 3;
    private JSONArray jsonFriendsArray;
    private PagerTabStrip pagerTabStrip;

    MyAdapter mAdapter;

    ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_pager);
        pagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_header);
        pagerTabStrip.setDrawFullUnderline(true);
        pagerTabStrip.setTabIndicatorColor(Color.RED);


        mAdapter = new MyAdapter(getSupportFragmentManager());

        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
    }


    public class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) { super(fm); }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    GroupFragmentTab groupFragmentTab = new GroupFragmentTab();
                    // return groupFragmentTab;
                case 1:
                    FriendsFragmentTab friendsFragmentTab = new FriendsFragmentTab();
                    return friendsFragmentTab;
                case 2:
/*
                    InvitesFragmentTab invitesFragmentTab = new InvitesFragmentTab();
                    return invitesFragmentTab;
*/

            }

            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String txt = "";
            switch (position) {
                case 0:
                    txt = "Groups";
                    break;
                case 1:
                    txt = "Friends";
                    break;
                case 2:
                    txt = "Invites";
                    break;
            }

            return txt;
        }
    }

    public void onFragmentInteraction(String id) {
        Log.i("implement", id);
    }



}
