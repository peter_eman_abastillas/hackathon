package com.nullified.cafeloiters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DecimalFormat;

/**
 * Created by PETEREMAN.ABASTILLAS on 6/5/2015.
 */
public class SurveyedLocation extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String DOUBLE_TYPE = " DOUBLE";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + SurveyEntry.TABLE_NAME + " (" +
                    SurveyEntry._ID + " INTEGER PRIMARY KEY," +
                    SurveyEntry.LOCATION_lAT_ENTRY_ID + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.LOCATION_LNG_ENTRY_ID + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.LOCATION_WARNING_TYPE + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.LOCATION_RATE + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.LOCATION_EMOJI_ENTRY_ID + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.LOCATION_PLACE_ID + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.LOCATION_ADD_ENTRY_ID + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.COSLAT + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.SINLAT + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.COSLNG + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.SINLNG + TEXT_TYPE + COMMA_SEP +
                    SurveyEntry.LOCATION_COMMENT_ENTRY_ID + TEXT_TYPE +
            " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + SurveyEntry.TABLE_NAME;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "SurveyedLocation2.db";

    public SurveyedLocation(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    public Cursor getNearLocation( double lat, double lng) {
        SQLiteDatabase db =  this.getReadableDatabase();
        DecimalFormat df = new DecimalFormat("0.0000");

        double rLat = (double) Math.floor(lat * 1000) / 1000;
        double rLng = (double) Math.floor(lng * 1000) / 1000;
        String selectQuery = "SELECT  * FROM " + SurveyEntry.TABLE_NAME;

        String cond = SurveyEntry.LOCATION_lAT_ENTRY_ID + " LIKE ? AND " +
                SurveyEntry.LOCATION_LNG_ENTRY_ID + " LIKE ? ";
        Log.i("QUERY???",  String.valueOf(rLat)+" "+String.valueOf(rLng));
        Cursor raw = db.rawQuery(selectQuery, null);

        return raw;
    }
    public static String buildDistanceQuery(double latitude, double longitude) {
        final double coslat = Math.cos(deg2rad(latitude));
        final double sinlat = Math.sin(deg2rad(latitude));
        final double coslng = Math.cos(deg2rad(longitude));
        final double sinlng = Math.sin(deg2rad(longitude));
        //@formatter:off
        return "(" + coslat + "*" + SurveyEntry.COSLAT
                + "*(" + SurveyEntry.COSLNG + "*" + coslng
                + "+" + SurveyEntry.SINLNG + "*" + sinlng
                + ")+" + sinlat + "*" + SurveyEntry.SINLAT
                + ")";
        //@formatter:on
    }
    public String[] injectLocationValues(double lat, double lng) {
        return new String[] {
                String.valueOf(lat),
                String.valueOf(lng),
                String.valueOf(Math.cos(deg2rad(lat))),
                String.valueOf(Math.sin(deg2rad(lat))),
                String.valueOf(Math.cos(deg2rad(lng))),
                String.valueOf(Math.sin(deg2rad(lng)))
        };
    }

    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    /* Inner class that defines the table contents */
    public static abstract class SurveyEntry implements BaseColumns {
        public static final String TABLE_NAME = "survey";
        public static final String LOCATION_lAT_ENTRY_ID = "lat";
        public static final String LOCATION_LNG_ENTRY_ID = "lng";
        public static final String LOCATION_WARNING_TYPE = "warning_type";
        public static final String LOCATION_RATE = "rate";
        public static final String LOCATION_EMOJI_ENTRY_ID = "emoji";
        public static final String LOCATION_PLACE_ID = "placeId";
        public static final String LOCATION_ADD_ENTRY_ID = "address";
        public static final String LOCATION_COMMENT_ENTRY_ID = "comment";
        public static final String COSLAT = "cos_lat";
        public static final String SINLAT = "sin_lat";
        public static final String COSLNG = "cos_lng";
        public static final String SINLNG = "sin_lng";
        public static final String COLUMN_NAME_NULLABLE = null;
    }
}
