package com.nullified.cafeloiters;
import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by PETEREMAN.ABASTILLAS on 6/13/2015.
 */
public class FriendsListFragment extends android.support.v4.app.ListFragment {
    String[] alphabet = new String[] { "A","B","C"};
    String[] word = new String[]{"Apple", "Boat", "Cat"};
    SCListAdapter newList;
    private String mTag;

    public FriendsListFragment() {
    }

    public FriendsListFragment(String tag) {
        mTag = tag;

        Log.d("GROUP_TAB", "Constructor: tag=" + tag);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.friends_fragment_list, container, false);
        try {
            getJsonExtra(rootView);
        } catch (JSONException e) {

        }
        return rootView;
    }

    public void getJsonExtra(final View rootView) throws JSONException {
        JSONArray jsonFriendsArray = new JSONArray("[{\"name\":\"Beverly Analupa Abastillas\",\"id\":\"10200488016200323\"}]");
        GraphRequest request1 = GraphRequest.newMyFriendsRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONArrayCallback() {
                    @Override
                    public void onCompleted(JSONArray jsonArray, GraphResponse graphResponse) {
                        JSONArray jsonFriendsArray = jsonArray;
                        GraphResponse graphFriendsResponse = graphResponse;
                        Log.v("FACEBOOK_DATA", jsonArray.toString());
                        Log.v("FACEBOOK_DATA", graphResponse.toString());
                        Log.v("FACEBOOK_DATA", this.getClass().toString() + " has invoke Facebook json");
                        populateUsersList(rootView, jsonArray);
                    }
                });
        request1.executeAsync();

        Log.e("json?", " " + jsonFriendsArray);

    }


    private void populateUsersList(View rootView, JSONArray jsonArray) {
        // Construct the data source
        ArrayList<SCListItems> arrayOfUsers = SCListItems.setUsers(jsonArray);
        Log.e("LIST", " "+ arrayOfUsers);
        newList = new SCListAdapter(getActivity(), arrayOfUsers, "Friends");
        // Create the adapter to convert the array to views
        // Attach the adapter to a ListView
        setListAdapter(newList);
        newList.notifyDataSetChanged();

    }

}