package com.nullified.cafeloiters;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by PETEREMAN.ABASTILLAS on 6/13/2015.
 */
public class SCListItems {

    private String itemTitle;

    public String name;
    public String id;

    public SCListItems(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public static ArrayList<SCListItems> setUsers(JSONArray fb) {
        ArrayList<SCListItems> users = new ArrayList<SCListItems>();
        try {
            for (int i = 0; i < fb.length(); ++i) {
                JSONObject rec = fb.getJSONObject(i);
                int id = rec.getInt("id");
                Log.i("SCListItems", rec.getString("id") + " " + rec.getString("name"));
                users.add(new SCListItems(rec.getString("name"), rec.getString("id")));
            }
        } catch (JSONException e) {

        }


        return users;
    }
    public static ArrayList<SCListItems> setGroups(JSONArray fb) {
        ArrayList<SCListItems> users = new ArrayList<SCListItems>();
        try {
            for (int i = 0; i < fb.length(); ++i) {
                JSONObject rec = fb.getJSONObject(i);
                //if(rec.getString("status") == "joined") {
                    Log.i("SCListItems", rec.getString("id") + " " + rec.getString("name"));
                    users.add(new SCListItems(rec.getString("name"), rec.getString("id")));
                // }
            }
        } catch (JSONException e) {

        }


        return users;
    }


    public static ArrayList<SCListItems> getUsers(JSONArray fb) {
        ArrayList<SCListItems> users = new ArrayList<SCListItems>();
        users.add(new SCListItems("Harry", "San Diego"));
        return users;
    }
}
